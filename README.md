Repository contains:

1. ERD.png ---> temporary ERD for dummy data, needs improvement. There are some confusions about the data to be stored in mongo database.

2. dummydata2.zip ---> dummy data.
importing the database (dummydata2) by extracting dummydata2 folder into "dump" folder of mongodb. Then in cmd of mongodb's bin folder, use "mongorestore" command to restore the database into mongodb.